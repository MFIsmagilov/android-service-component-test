package com.example.servicetest

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initBtn.setOnClickListener {
            Singleton.getInstance()
        }

        recreateBtn.setOnClickListener {
            Singleton.reCreate()
        }

        startServiceOnOtherProcessBtn.setOnClickListener {
            startService(Intent(this, MyServiceOnOtherProcess::class.java))
        }

        startServiceOnCurrentProcessBtn.setOnClickListener {
            startService(Intent(this, MyServiceOnCurrentProcess::class.java))
        }
    }
}
