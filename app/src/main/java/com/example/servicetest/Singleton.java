package com.example.servicetest;

import android.util.Log;

import java.util.Random;

public class Singleton {

    private int id;

    public int getId() {
        return id;
    }

    private Singleton(int id) {
        this.id = id;
    }

    private static volatile Singleton INSTANCE;


    public static Singleton getInstance() {
        Singleton localInstance = INSTANCE;
        if (localInstance == null) {
            synchronized (Singleton.class) {
                localInstance = INSTANCE;
                if (localInstance == null) {
                    Random random = new Random(System.currentTimeMillis());
                    int number = random.nextInt();
                    Log.d("Singleton", "init with number = " + number);
                    INSTANCE = localInstance = new Singleton(number);
                }
            }
        }
        return localInstance;
    }

    public static Singleton reCreate() {
        synchronized (Singleton.class) {
            INSTANCE = null;
            getInstance();
        }
        return getInstance();
    }
}
