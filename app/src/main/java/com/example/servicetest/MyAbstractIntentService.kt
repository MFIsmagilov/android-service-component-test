package com.example.servicetest

import android.app.IntentService
import android.content.Intent
import android.util.Log

abstract class MyAbstractIntentService(name: String) : IntentService(name) {
    var TAG = this::class.java.simpleName

    override fun onCreate() {
        super.onCreate()
        Log.i(TAG, "onCreate")
    }

    override fun onHandleIntent(intent: Intent?) {
        Log.i(TAG, "start long work")
        Thread.sleep(1000)
        Log.i(TAG, "stop long work")
        Log.d(TAG, "id = " + Singleton.getInstance().id)
        stopSelf()
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i(TAG, "onDestroy")
    }

    override fun onBind(intent: Intent) = null
}
