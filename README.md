## Тестовый проект

Чтобы посмотреть работу сервисов в разных процессах

![](https://bitbucket.org/MFIsmagilov/android-service-component-test/raw/a8fd2350c9442fce40c37d2ddb700ddf5bdf5231/screenshots/screen.jpg)


## Что такое id в логах?

Смотри на класс Singleton


```Java
public class Singleton {

    private int id;

    public int getId() {
        return id;
    }
	...
}	
```

## Сервис в текущем процессе

В файле AndroidManifes.xml зарегистрирован так:

```xml
<service
	android:name=".MyServiceOnCurrentProcess"/>
```

### Log

```
	MyApp: onCreate	//start app
[!]	Singleton: init with number = -628988259 //init singleton button click
	MyServiceOnCurrentProcess: onCreate 	//start service on current process
	MyServiceOnCurrentProcess: start long work
	MyServiceOnCurrentProcess: stop long work
	MyServiceOnCurrentProcess: id = -628988259
	MyServiceOnCurrentProcess: onDestroy
[!]	Singleton: init with number = 1141641504 //recreate singleton button click
	MyServiceOnCurrentProcess: onCreate		//start service on current process
	MyServiceOnCurrentProcess: start long work
	MyServiceOnCurrentProcess: stop long work
	MyServiceOnCurrentProcess: id = 1141641504
	MyServiceOnCurrentProcess: onDestroy
```


## Сервис в процессе :MyServiceOnOtherProcess

В файле AndroidManifes.xml зарегистрирован так:

```xml
<service
	android:name=".MyServiceOnOtherProcess"
	android:process=":MyServiceOnOtherProcess"/>
```

### Log

```
//в процессе приложения
	MyApp: onCreate 
[!]	Singleton: init with number = -429034920//init singleton button click

//в процессе :MyServiceOnOtherProcess
	
	MyApp: onCreate 
	MyServiceOnOtherProcess: onCreate
	MyServiceOnOtherProcess: start long work
	MyServiceOnOtherProcess: stop long work
[!]	Singleton: init with number = -1845852225
[!]	MyServiceOnOtherProcess: id = -1845852225
	MyServiceOnOtherProcess: onDestroy

//в процессе приложения
	
[!]	Singleton: init with number = -1284210776 //после того как сервис onDestroy нажимается RECREATE SINGLETON
	//затем нажимается START SERVICE ON OTHER PROCESS

//в процессе :MyServiceOnOtherProcess	
	MyServiceOnOtherProcess: onCreate
	MyServiceOnOtherProcess: start long work
	MyServiceOnOtherProcess: stop long work
[!]	MyServiceOnOtherProcess: id = -1845852225
	MyServiceOnOtherProcess: onDestroy
```

